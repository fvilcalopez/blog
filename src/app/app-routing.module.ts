import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAuthorComponent } from './auth/add-author/add-author.component';
import { AddBlogComponent } from './auth/add-blog/add-blog.component';
import { AddNewBlogComponent } from './auth/add-new-blog/add-new-blog.component';
// import { AddBlogComponent } from './auth/add-blog/add-blog.component';

import { AddProgramaComponent } from './auth/add-programa/add-programa.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { BlogComponent } from './pages/blog/blog.component';
import { CalidadComponent } from './pages/calidad/calidad.component';
import { ConstruccionComponent } from './pages/construccion/construccion.component';

// import { HomeComponent } from './pages/home/home.component';
import { LogisticaComponent } from './pages/logistica/logistica.component';
import { MantenimientoComponent } from './pages/mantenimiento/mantenimiento.component';
import { MineriaComponent } from './pages/mineria/mineria.component';
import { PagesComponent } from './pages/pages.component';
import { SearhComponent } from './pages/searh/searh.component';
import { SeguridadComponent } from './pages/seguridad/seguridad.component';
import {PageDetailComponent} from './pages/page-detail/page-detail.component';

const routes: Routes = [


    {
      path: 'blog',
      component: PagesComponent,
      children: [
        // { path:'home', component:HomeComponent },
        // { path: 'calidad', component: CalidadComponent},
        { path: ':page', component: PageDetailComponent},
        // // { path: 'detalle/:id' , component:BlogComponent},
        // // { path:'calidad/:id',component:BlogComponent},
        // { path: 'construccion', component: ConstruccionComponent },
        // // { path:'construccion/:id',component:BlogComponent},
        // { path: 'logistica', component: LogisticaComponent },
        // // { path:'logistica/:id',component:BlogComponent},
        // { path: 'mantenimiento', component: MantenimientoComponent },
        // // { path:'mantenimiento/:id',component:BlogComponent},
        // { path: 'mineria', component: MineriaComponent},
        // // { path:'mineria/:id',component:BlogComponent},
        // { path: 'seguridad', component: SeguridadComponent },
        // // { path:'seguridad/:id',component:BlogComponent},
        { path: 'search/:id', component: SearhComponent},
        { path: 'detalle/:id', component: BlogComponent},
        { path: '', redirectTo: '/calidad', pathMatch: 'full' },
        // { path:'**' ,redirectTo:'/calidad',pathMatch:'full' }
      ],

    },
    // { path:'addBlog',component:AddBlogComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
  //   {path:'app',children:[
  //     { path:'addPrograma', component:AddProgramaComponent },
  //     { path:'addAuthor', component:AddAuthorComponent },
  //     { path:'addNewBlog',component:AddNewBlogComponent},
  //     { path:'addBlog',component:AddBlogComponent},
  //   ]
  // }



    // { path:'', redirectTo:'blog',pathMatch:'full' },
   // { path:'**' ,component:NopagefoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
