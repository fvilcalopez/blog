import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component'
import { PagesModule } from './pages/pages.module';
import { ComponentsModule } from './components/components.module';

import { AddProgramaComponent } from './auth/add-programa/add-programa.component';

import { AddAuthorComponent } from './auth/add-author/add-author.component';
import { AddBlogComponent } from './auth/add-blog/add-blog.component';
import { AddNewBlogComponent } from './auth/add-new-blog/add-new-blog.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AddProgramaComponent,
    AddAuthorComponent,
    AddBlogComponent,
    AddNewBlogComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    PagesModule,
    ComponentsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
