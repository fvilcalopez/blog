import { Component, OnInit } from '@angular/core';
import {Blog} from '../../interfaces/blog-response';
import {PruebaService} from '../../services/prueba.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-page-detail',
  templateUrl: './page-detail.component.html',
  styleUrls: ['./page-detail.component.css']
})
export class PageDetailComponent implements OnInit {

  public blogs: Blog[] = [];
  category = '';

  idCat = 1;
  constructor(private prueba: PruebaService,
              private activatedRoute: ActivatedRoute) {
    this.category = this.activatedRoute.snapshot.params.page;
    console.log(this.category);
  }

  ngOnInit(): void {
    switch (this.category) {
      case 'construccion':
        this.idCat = 1;
        break;
      case 'mantenimiento':
        this.idCat = 2;
        break;
      case 'mineria':
        this.idCat = 3;
        break;
      case 'calidad':
        this.idCat = 4;
        break;
      case 'logistica':
        this.idCat = 5;
        break;
      case 'seguridad':
        this.idCat = 6;
        break;
    }
  }

}
