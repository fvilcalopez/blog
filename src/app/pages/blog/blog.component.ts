import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PruebaService } from '../../services/prueba.service';
import { Blog } from 'src/app/interfaces/blog-response';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  idCat:string='';

  public blog!: Blog;
  public topBlogs!: Blog[];
  id:number;
  constructor( private activatedRoute: ActivatedRoute,
               private   prueba: PruebaService
              ) {

                this.activatedRoute.params.subscribe( parametros =>{
                this.id = parametros.id



               })
                this.id = this.activatedRoute.snapshot.params.id;
               }

  ngOnInit(): void {
  //  const id = this.activatedRoute.snapshot.params.id;
    console.log(this.id, 'Desde Blog');
    this.prueba12();
  }

  prueba12() {
    console.log(this.id, 'Desde Blog');
    this.prueba.getBlogById(this.id).subscribe( resp => {
        this.blog = resp;
        this.idCat = this.blog.idCategory;


        this.prueba.getTop2ByCategory(this.idCat)
        .subscribe(resp => {
          this.topBlogs = resp;
          console.log(this.topBlogs);
        })
    })
  }

}

