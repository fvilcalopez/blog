import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-mineria',
  templateUrl: './mineria.component.html',
  styleUrls: ['./mineria.component.css']
})
export class MineriaComponent implements OnInit {

  public blogs : Blog[]=[];

  idCat:number=3;
  constructor(private prueba : PruebaService) { }

  ngOnInit(): void {


    this.prueba.getCategoriforID(this.idCat).subscribe( resp => {
      this.blogs = resp;
      console.log(this.blogs,'blogs arles');

    })
  }
}
