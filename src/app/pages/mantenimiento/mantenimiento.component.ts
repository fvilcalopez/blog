import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.css']
})
export class MantenimientoComponent implements OnInit {


  public blogs : Blog[]=[];

  idCat:number=1;
  constructor(private prueba : PruebaService) { }

  ngOnInit(): void {


    this.prueba.getCategoriforID(this.idCat).subscribe( resp => {
      this.blogs = resp;
      console.log(this.blogs,'blogs arles');

    })
  }
}
