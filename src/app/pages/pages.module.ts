import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { CalidadComponent } from './calidad/calidad.component';
import { ConstruccionComponent } from './construccion/construccion.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { LogisticaComponent } from './logistica/logistica.component';
import { SeguridadComponent } from './seguridad/seguridad.component';
import { MineriaComponent } from './mineria/mineria.component';
import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';

import { AppRoutingModule } from '../app-routing.module';

import { ComponentsModule } from '../components/components.module';
import { BlogComponent } from './blog/blog.component';
import { SearhComponent } from './searh/searh.component';
import { PageDetailComponent } from './page-detail/page-detail.component'



@NgModule({
  declarations: [
    HomeComponent,
    CalidadComponent,
    ConstruccionComponent,
    MantenimientoComponent,
    LogisticaComponent,
    SeguridadComponent,
    MineriaComponent,
    PagesComponent,
    BlogComponent,
    SearhComponent,
    PageDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    ComponentsModule


  ],
  exports:[
    HomeComponent,
    CalidadComponent,
    ConstruccionComponent,
    MantenimientoComponent,
    LogisticaComponent,
    SeguridadComponent,
    MineriaComponent,
    BlogComponent
  ]
})
export class PagesModule { }
