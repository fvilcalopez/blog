import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-construccion',
  templateUrl: './construccion.component.html',
  styleUrls: ['./construccion.component.css']
})
export class ConstruccionComponent implements OnInit {

  public blogs: Blog[]=[];

  idCat:number=2;
  constructor(private prueba : PruebaService) { }

  ngOnInit(): void {


    this.prueba.getCategoriforID(this.idCat).subscribe( resp => {
      this.blogs = resp;
      console.log(this.blogs,'blogs arles');

    })
  }
}
