import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-searh',
  templateUrl: './searh.component.html',
  styleUrls: ['./searh.component.css']
})
export class SearhComponent implements OnInit {

  blogs:Blog[] = [];
  texto:string ='';
  constructor( private pruebaService : PruebaService,
                private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      this.activatedRoute.params.subscribe(params =>{
        console.log(params.id);

        this.pruebaService.getSearchByText( params.id )
        .subscribe( resp =>{
          console.log(resp);

          this.blogs = resp;
          console.log(this.blogs);

        })
      } )
  }
}
