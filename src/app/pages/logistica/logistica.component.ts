import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-logistica',
  templateUrl: './logistica.component.html',
  styleUrls: ['./logistica.component.css']
})
export class LogisticaComponent implements OnInit {


  public blogs : Blog[]=[];

  idCat:number=5;
  constructor(private prueba : PruebaService) { }

  ngOnInit(): void {


    this.prueba.getCategoriforID(this.idCat).subscribe( resp => {
      this.blogs = resp;
      console.log(this.blogs,'blogs arles');

    })
  }
}
