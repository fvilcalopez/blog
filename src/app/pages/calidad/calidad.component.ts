
import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';
import { PruebaService } from 'src/app/services/prueba.service';

@Component({
  selector: 'app-calidad',
  templateUrl: './calidad.component.html',
  styleUrls: ['./calidad.component.css']
})
export class CalidadComponent implements OnInit {

  public blogs : Blog[]=[];

  idCat:number=4;
  constructor(private prueba : PruebaService) { }

  ngOnInit(): void {


    this.prueba.getCategoriforID(this.idCat).subscribe( resp => {
      this.blogs = resp;
      console.log(this.blogs,'blogs arles');

    })
  }
}
