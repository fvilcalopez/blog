import { TestBed } from '@angular/core/testing';

import { ActiveCamService } from './active-cam.service';

describe('ActiveCamService', () => {
  let service: ActiveCamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActiveCamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
