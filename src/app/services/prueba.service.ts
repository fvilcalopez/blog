import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs';

import { Blog } from 'src/app/interfaces/blog-response';
import { AuthorModel } from '../model/author.model';


@Injectable({
  providedIn: 'root'
})


export class PruebaService {

  constructor(  private http: HttpClient) {

  }


  getCategoriforID(idCat:number ):Observable<Blog[]>{
    return this.http.get<Blog[]>(`https://ingenium.edu.pe/blogbackend/index.php?command=blog_get_by_category&id=${idCat}`)

    //return this.http.get<blog>('https://ingenium.edu.pe/blogbackend/index.php?command=blog_get_by_id&id=1')
    //return this.http.get(`https://api.themoviedb.org/3/movie/popular?api_key=37a217ecbd64a1710e6fc815d0f85938&language=en-ES&page=1`)
   }

  getBlogById(id :number):Observable<Blog>{
     return this.http.get<Blog>(`https://ingenium.edu.pe/blogbackend/index.php?command=blog_get_by_id&id=${ id }`)
   }

  getTop2ByCategory(id:string):Observable<Blog[]>{
     return this.http.get<Blog[]>(`https://ingenium.edu.pe/blogbackend/index.php?command=top2_blogs&categoryId=${id}`)
   }
  getSearchByText(text:string ):Observable<Blog[]>{
    return this.http.get<Blog[]>(`https://ingenium.edu.pe/blogbackend/index.php?command=blog_search&keyword=${ text }`)
  }

  addAthorBlog(form:AuthorModel ){
    return this.http.post(`https://ingenium.edu.pe/blogbackend/index.php?command=author-create`,{data: form} )
  }
}
