export  interface BlogsResponse{
  results: Blog[];
}


export interface Blog {

  resumen:             string,
  idBlog:              string;
  idCategory:          string;
  titulo:              string;
  contenido:           string;
  imgBlogURL:          string;
  fecha:               Date;
  numeroLike:          number;
  categoria:           string;
  etiquetas:           string;
  autorGrado:          string;
  autorName:           string;
  autorCargo:          string;
  autorImgURL:         string;
  numComen:            string;
  linkPrograma:        string;
  imgPrograma:         string;
  descriptionPrograma: string;
  fechaInicioPrograma: Date;
  nombrePrograma: string;

}
