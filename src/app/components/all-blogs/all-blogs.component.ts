import { stringify } from '@angular/compiler/src/util';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Blog } from 'src/app/interfaces/blog-response';

@Component({
  selector: 'app-all-blogs',
  templateUrl: './all-blogs.component.html',
  styleUrls: ['./all-blogs.component.css']
})
export class AllBlogsComponent implements OnInit {
  @Input()
  blogs!: Blog[];
  area:string='3';
  constructor( private router: Router ) { }

  ngOnInit(): void {
    // console.log(this.blogs[0].idCategory,'todos lso blog ');

    // this.blogs[0].idCategory

    // console.log(this.blogs, 'Dedes tarjeta');
    //  if(this.blogs[0].idCategory== 1)
    //       this.area='mantenimiento';
    //   if(this.blogs[0].idCategory== 2)
    //       this.area='construccion';
    //   if(this.blogs[0].idCategory== 3)
    //       this.area='mineria';
    //   if(this.blogs[0].idCategory== 4)
    //       this.area='calidad';
    //   if(this.blogs[0].idCategory== 5)
    //       this.area='logistica';
    //   if(this.blogs[0].idCategory== 6)
    //       this.area='seguridad';

  }


  onDetalleBlog(blog: Blog){
    if( blog.idCategory == '1')
        this.area='mantenimiento';
    if(blog.idCategory == '2')
        this.area='construccion';
    if(blog.idCategory== '3')
        this.area='mineria';
    if(blog.idCategory== '4')
        this.area='calidad';
    if(blog.idCategory== '5')
        this.area='logistica';
    if(blog.idCategory== '6')
        this.area='seguridad';
    console.log(blog, 'Desde all Blog');
    this.router.navigate([`detalle/`,blog.idBlog ]);


  }

}
