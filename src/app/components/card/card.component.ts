
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Blog } from 'src/app/interfaces/blog-response';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() topBlogs:Blog[]=[];
  @Output() propagar = new EventEmitter<string>();
  area:string ='';

  constructor( private router:Router,
               private activatedRoute:ActivatedRoute ) {
                 this.activatedRoute.params.subscribe( resp =>{
                    console.log(resp ,'desde el hijo ');

                 })
   }


  ngOnInit(): void {

  }
  ngOnChanges(){
    console.log(this.topBlogs, 'by onchange');

  }


  onCategoryBlog(blog: Blog){
    if( blog.idCategory == '1')
        this.area='mantenimiento';
    if(blog.idCategory == '2')
        this.area='construccion';
    if(blog.idCategory== '3')
        this.area='mineria';
    if(blog.idCategory== '4')
        this.area='calidad';
    if(blog.idCategory== '5')
        this.area='logistica';
    if(blog.idCategory== '6')
        this.area='seguridad';
    console.log(blog, 'Desde all Blog',this.area,blog.idBlog);
    this.router.navigate([`${ this.area }` ]);
  }

  detalleBlog(blog: Blog){
    if( blog.idCategory == '1')
        this.area='mantenimiento';
    if(blog.idCategory == '2')
        this.area='construccion';
    if(blog.idCategory== '3')
        this.area='mineria';
    if(blog.idCategory== '4')
        this.area='calidad';
    if(blog.idCategory== '5')
        this.area='logistica';
    if(blog.idCategory== '6')
        this.area='seguridad';
    console.log(blog, 'Desde Card', this.area,blog.idBlog);

    this.router.navigate([`detalle`,blog.idBlog ]);
    this.propagar.emit('actualizar');
  }
  // ${this.area}

}
