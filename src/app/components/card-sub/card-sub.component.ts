import { Component, OnInit } from '@angular/core';
import { ActiveCamService } from 'src/app/services/active-cam.service';

@Component({
  selector: 'app-card-sub',
  templateUrl: './card-sub.component.html',
  styleUrls: ['./card-sub.component.css']
})
export class CardSubComponent implements OnInit {
  // private actionService:ActiveCamService

  constructor( private actionService: ActiveCamService ) { }

  ngOnInit(): void {
    // this.actionService.setSubcrip("Ariel","Ariel@gmail.com").subscribe( resp =>{
    //   console.log(resp);
    // })
  }

  // sendSuscri(name:string , email:string){
  //   this.actionService.setSubcrip(name,email).subscribe( resp =>{
  //     console.log(resp);
  //   })
  // }
  sendSub(name:string, email:string){

    this.actionService.setSubcrip(name,email).subscribe( resp =>{
   console.log(resp);
    })
 }

}
