import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSubComponent } from './card-sub.component';

describe('CardSubComponent', () => {
  let component: CardSubComponent;
  let fixture: ComponentFixture<CardSubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardSubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
