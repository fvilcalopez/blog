import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { CardProgramaComponent } from './card-programa/card-programa.component';
import { CardSubComponent } from './card-sub/card-sub.component';
import { BanerComponent } from './baner/baner.component';
import { CuerpoComponent } from './cuerpo/cuerpo.component';
import { AllBlogsComponent } from './all-blogs/all-blogs.component';



@NgModule({
  declarations: [
    CardComponent,
    CardProgramaComponent,
    CardSubComponent,
    BanerComponent,
    CuerpoComponent,
    AllBlogsComponent
  ],
  imports: [
    CommonModule,

  ],
  exports:[
    CardComponent,
    CardProgramaComponent,
    CardSubComponent,
    BanerComponent,
    CuerpoComponent,
    AllBlogsComponent
  ]
})
export class ComponentsModule { }
