import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog-response';

@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CuerpoComponent implements OnInit {
  private someHtmlCode = '';
  @Input()
  blog!: Blog;

  contenido : string ='';
  constructor( private renderer:Renderer2 ) {

  }
  ngOnInit(): void {
    this.contenido =this.blog.contenido
    console.log(this.contenido);
  }
  @ViewChild('one')
  d1!: ElementRef;
  ngAfterViewInit() {
    this.d1.nativeElement.insertAdjacentHTML('beforeend',`${ this.contenido }`);
  }
}
