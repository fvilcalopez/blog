import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Blog } from '../../interfaces/blog-response';


@Component({
  selector: 'app-card-programa',
  templateUrl: './card-programa.component.html',
  styleUrls: ['./card-programa.component.css']
})
export class CardProgramaComponent implements OnInit {
  @Input()
  blog!: Blog;
 area:string='';
  constructor(private  router : Router) { }

  ngOnInit(): void {
    console.log(this.blog.linkPrograma);

  }
  onDetailCat(blog: Blog){
    if( blog.idCategory == '1')
        this.area='mantenimiento';
    if(blog.idCategory == '2')
        this.area='construccion';
    if(blog.idCategory== '3')
        this.area='mineria';
    if(blog.idCategory== '4')
        this.area='calidad';
    if(blog.idCategory== '5')
        this.area='logistica';
    if(blog.idCategory== '6')
        this.area='seguridad';
    console.log(blog, 'Desde all Blog',this.area,blog.idBlog);

    window.location.href=`${blog.linkPrograma}`;
  }
  onCategoryBlog(blog: Blog){
    if( blog.idCategory == '1')
        this.area='mantenimiento';
    if(blog.idCategory == '2')
        this.area='construccion';
    if(blog.idCategory== '3')
        this.area='mineria';
    if(blog.idCategory== '4')
        this.area='calidad';
    if(blog.idCategory== '5')
        this.area='logistica';
    if(blog.idCategory== '6')
        this.area='seguridad';
    console.log(blog, 'Desde all Blog',this.area,blog.idBlog);
    this.router.navigate([`${ this.area }` ]);
  }
}
