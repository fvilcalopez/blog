import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardProgramaComponent } from './card-programa.component';

describe('CardProgramaComponent', () => {
  let component: CardProgramaComponent;
  let fixture: ComponentFixture<CardProgramaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardProgramaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
