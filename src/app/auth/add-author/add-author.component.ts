import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthorModel } from 'src/app/model/author.model';
import { PruebaService } from 'src/app/services/prueba.service';
import { FormBuilder, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent implements OnInit {

  author: AuthorModel = new AuthorModel;


  constructor( private pruebaService: PruebaService ) {

              }






  ngOnInit(): void {

  }
  guardar( form1:NgForm ){
    console.log(form1 );
    console.log( this.author);
    this.pruebaService.addAthorBlog( this.author ).subscribe( resp =>{
      console.log(resp);

    })
  }
}
