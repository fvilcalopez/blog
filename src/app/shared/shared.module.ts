import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarPrimarioComponent } from './navbar-primario/navbar-primario.component';
import { NavbarSecundarioComponent } from './navbar-secundario/navbar-secundario.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    NavbarPrimarioComponent,
    NavbarSecundarioComponent,
    BreadcrumbsComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    NavbarPrimarioComponent,
    NavbarSecundarioComponent,
    BreadcrumbsComponent,
    FooterComponent
  ]
})
export class SharedModule { }
