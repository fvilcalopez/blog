import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarPrimarioComponent } from './navbar-primario.component';

describe('NavbarPrimarioComponent', () => {
  let component: NavbarPrimarioComponent;
  let fixture: ComponentFixture<NavbarPrimarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarPrimarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarPrimarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
