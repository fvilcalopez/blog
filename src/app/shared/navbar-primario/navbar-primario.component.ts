import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-primario',
  templateUrl: './navbar-primario.component.html',
  styleUrls: ['./navbar-primario.component.css']
})
export class NavbarPrimarioComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    var menuBox=document.getElementById('menuToggle');
    //var header=document.getElementById('header');
    var navMobile=document.getElementById('navMobile');

    if(menuBox != null) {
        menuBox.addEventListener('click', function(){
        'use strict';
        if(menuBox!=null && navMobile!=null){
          menuBox.classList.toggle('isActive');
          // header.classList.toggle('noneOpacity');
          navMobile.classList.toggle('stretchMenu');
        }
        });
    }

    const itemsMobile=document.querySelectorAll(".headerItemMobile");

    itemsMobile.forEach(itemMenuChoose => {
    itemMenuChoose.addEventListener('click', function(){
        const currentItem=document.querySelector(".headerItemMobile.stretchMenu");
        if(currentItem && currentItem!=itemMenuChoose){
            currentItem.classList.toggle('stretchMenu');
        }
        itemMenuChoose.classList.toggle('stretchMenu');
    })
    });

    const subItems=document.querySelectorAll(".subItHead");

    subItems.forEach(itemMenuChoose => {
    itemMenuChoose.addEventListener('click', function(){



        const currentItem=document.querySelector(".subItHead.stretchMenu");
        if(currentItem && currentItem!=itemMenuChoose){
            currentItem.classList.toggle('stretchMenu');
        }
        itemMenuChoose.classList.toggle('stretchMenu');
    })
    });

  }
}
