import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-secundario',
  templateUrl: './navbar-secundario.component.html',
  styleUrls: ['./navbar-secundario.component.css']
})
export class NavbarSecundarioComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  searchBlog(textoBuscar: string): void{
    textoBuscar = textoBuscar.trim();
    if ( textoBuscar.length === 0) {
      return;
    }
    this. router.navigate(['/search/', textoBuscar]);
  }

  irNavegacion(page: string): void {
    this.router.navigate(['./blog/' + page]);
  }
}
