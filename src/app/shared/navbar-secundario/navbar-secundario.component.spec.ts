import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarSecundarioComponent } from './navbar-secundario.component';

describe('NavbarSecundarioComponent', () => {
  let component: NavbarSecundarioComponent;
  let fixture: ComponentFixture<NavbarSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
